function _kcs_wait() {
	if (window.jQuery) {
		if (window.jQuery.csv) {
			_kcs_survey();
		} else {
			_kcs(_kcs_jquery_csv);
			window.setTimeout(_kcs_wait, 50);
		}
	} else {
		window.setTimeout(_kcs_wait, 50);
	}
}
_kcs_wait();

function _kcs_survey() {
	(function (jQuery) { 
		var RBPopupClass = function (el, opts) {
	        var element = jQuery(el);
	        var options = opts;

			jQuery(element).html('<div class="rb-arrow-btn show">-</div>');

			var choices = new Array();
			if (options.is_completed){
				jQuery(element).append('<div class="rb-completed">Thank you!</div>');
				setTimeout(
					function(){
						jQuery(element).animate({
							opacity:0
						}, 1000, function() {
							jQuery(element).html("");
						});
					}, 
					5000
				);
			}else{
				jQuery(element).append('<div class="rb-header">' + options.data.title + '</div>');
					
				if (options.data.value == "null" ){
					jQuery(element).append('<textarea class="rb-answer"></textarea>');
				}else{
					choices = options.data.value.split(",");
					for (i = 0; i < choices.length - 1; i++ ){
						if (i == 0 ){
							jQuery(element).append('<div class="rb-options selected">' + choices[i] + '</div>');
						}else{
							jQuery(element).append('<div class="rb-options">' + choices[i] + '</div>');
						}
					}
				}
			}
		
			jQuery(element).append('<a href="http://keepify.com/survey-landing.html"><div class="rb-license">Powered by Keepify</div></a>');

			var max_height
			var min_height = 35;

			if (options.is_completed ){
				max_height = 60;
				min_height = 20;
			}else{
				if (options.is_final){
					jQuery(element).append('<div class="rb-action">Send</div>');
				}else{
					jQuery(element).append('<div class="rb-action">Next</div>');
				}

				jQuery(".rb-action").on("click", function(){
					handleAction();
				});

				if (options.data.value == "null" ){
					max_height = 190;
				}else{
					max_height = 70 + 46 * (options.data.value.split(",").length - 1);
				}
				//70 + 46 * (choices.length - 1);
			}
				

			jQuery(element).css("width", "250px" );
			jQuery(element).css("padding", "10px" );
			jQuery(element).css("position", "fixed" );
			jQuery(element).css("bottom", "0px" );
			jQuery(element).css("right", options.pos_right + "px" );

			if (options.theme == "black" ){
				jQuery(element).addClass("black-popup");
			}else if (options.theme == "yellow" ){
				jQuery(element).addClass("yellow-popup");
			}else if (options.theme == "blue" ){
				jQuery(element).addClass("blue-popup");
			}else{
				jQuery(element).addClass("black-popup");
			}
       	
			if (options.is_open){
				jQuery(element).css("height", max_height + "px" );
				jQuery(".rb-arrow-btn").removeClass("show");
				jQuery(".rb-arrow-btn").addClass("close");
				jQuery(".rb-arrow-btn").html("×");
			}else{
				jQuery(element).css("height", min_height + "px" );
				jQuery(".rb-arrow-btn").removeClass("close");
				jQuery(".rb-arrow-btn").addClass("show");
				jQuery(".rb-arrow-btn").html("-");

				setTimeout(
					function(){
					jQuery(".rb-answer").show();
						if (jQuery(element).height() < (min_height + 1) ){
							jQuery(element).animate({
								height: 10
							}, 100, function() {
								jQuery(".rb-arrow-btn").removeClass("show");
								jQuery(".rb-arrow-btn").addClass("close");
								jQuery(".rb-arrow-btn").html("×");
								jQuery(element).animate({
									height: max_height
								}, options.animation_time, function() {
								// Animation complete.
								});
							});
						}
					}, 
					10000
				);
			}

	        var clickOptions = function (e) {
				if (options.is_multiple){
					if (jQuery(e.target).hasClass("selected")){
						jQuery(e.target).removeClass("selected");
					}else{
						jQuery(e.target).addClass("selected");
					}
				}else{
					jQuery(".rb-options").removeClass("selected" );
					jQuery(e.target).addClass("selected");
				}			

				return false;
	        };

			var popupAnimation = function(e ){
	          if (jQuery(e.target).hasClass("show")){
					jQuery(e.target).removeClass("show");
					jQuery(e.target).addClass("close");
					jQuery(e.target).html("×");
					showPopupWindow();
			  }else{
					jQuery(e.target).removeClass("close");
					jQuery(e.target).addClass("show");
					jQuery(e.target).html("-");
					hidePopupWindow();
			  }
				return false;
			};

			var hidePopupWindow = function(){
				jQuery(element).animate({
					height: max_height + 20
				}, 100, function() {
					jQuery(element).animate({
						height: min_height
					}, options.animation_time, function() {
						jQuery(".rb-answer").css("display", "none");
					// Animation complete.
					});
				});
				return false;
			};

			var showPopupWindow = function(){
				jQuery(".rb-answer").css("display", "block");
				jQuery(element).animate({
					height: 10
				}, 100, function() {
					jQuery(element).animate({
						height: max_height
					}, options.animation_time, function() {

					// Animation complete.
					});
				});
			};

	        jQuery(element).find('.rb-arrow-btn').bind('click', popupAnimation);
	        jQuery(element).find('.rb-options').bind('click', clickOptions);
        
	    };

	    jQuery.fn.RBPopup = function (options) {
	        var opts = jQuery.extend({}, jQuery.fn.RBPopup.defaults, options);

	        return this.each(function () {
	            new RBPopupClass(jQuery(this), opts);
	        });
	    }

	    jQuery.fn.RBPopup.defaults = {
			pos_right: 70,
	        theme: "black",
			data: Object(),
			animation_time: 500,
			is_multiple: true,
			is_final: false,
			is_open: false,
			is_completed: false
	    };

	})(jQuery);

	function getSelectedAnswer(){
		
		var ret = jQuery(".rb-answer").val();
		if (ret == null){
			var objs = jQuery(".rb-options");
			var tmp_obj = new Object();
			for (var i = 0; i < objs.length; i++ ){
				if (jQuery(objs[i]).hasClass("selected") ){
					ret = jQuery(objs[i]).html();
					break;
				}
			}
		}
		return ret;
	}
	
	function _kcs_css(u) {
	    setTimeout(function() {
	        var s = document.createElement('link');
	        var f = document.getElementsByTagName('script')[0];
	        s.type = 'text/css';
	        s.rel = "stylesheet";
			s.async = true;
			s.href = u;
	        f.parentNode.appendChild(s);
	        }, 10);
	}

	_kcs_css("https://s3.amazonaws.com/customer_surveys/rb-style.css");
	
	var popupDiv = document.createElement('div');
	popupDiv.className = "rb-popup";
	jQuery("body").append(popupDiv);
	
	var _kcs_question_url = "https://docs.google.com/spreadsheet/pub?key="+_kcs_key+"&single=true&gid=0&output=csv";

	var header_title_arr = new Array();
	var question_arr = new Array();
	var tester_arr = {};

	var index = 0;

	var results = new Array();
	var quest_len = 0;

	jQuery.ajax(_kcs_question_url).done(function(result){
		var datas = jQuery.csv.toObjects(result);
		
		if (datas.length == 0 && (result.split("\n").length == 1 && result.split(',').length > 0)) {
			quest_len = result.split(',').length
			for (var i in result.split(',')) {
				question_arr[result.split(',')[i]] = "null";
			}
		} else {
			for (var i = 0; i < datas.length; i++){
				quest_len = 0;
				for (var data in datas[i]){
					quest_len ++;
					var tmp = question_arr[data];
					if (tmp == null){
						tmp = "";
					}

					if (datas[i][data] != null && datas[i][data] != "" ){
						tmp = tmp + datas[i][data] + ",";
						question_arr[data] = tmp;
					}else{
						if (question_arr[data] == "" || question_arr[data] == "null" || question_arr[data] == null){
							question_arr[data] = "null";
						}
					}
				}
			}
		}
		initPopupWindow();
	});

	function getObj(obj, ind) {

		var tmp_ind = 0;
		var ret_obj = new Object();

		for (var key in obj){
			if(tmp_ind == ind ){
				ret_obj.title = key;
				ret_obj.value = obj[key];
				return ret_obj;
			}
			tmp_ind++;
		}
		return ret_obj;
	}

	function initPopupWindow(){
		jQuery(".rb-popup").RBPopup({theme:_kcs_theme, animation_time: 300, data:getObj(question_arr, 0), pos_right: 80, is_multiple: false, is_open: 
		false});
		index++;
	}

	function handleAction(){
		
		var ans = getSelectedAnswer();
		var obj = new Object();
		obj.question = getObj(question_arr, (index - 1) ).title;
		obj.answer = ans;
		results.push(obj );
	
		if (jQuery(".rb-action").html() == "Send" ){
			var ans = "";
			for (var i = 0; i < results.length; i++ ){
				ans = ans + results[i].answer + "-";
			}
			ans = ans + "Thank you!";
			var url = _kcs_answer_url + "?a=" + ans;
			jQuery.ajax(url, {
				type: 'GET',
				dataType: 'jsonp',
				success: function () {},
				error: function () {},
				});
			jQuery(".rb-popup").RBPopup({theme:_kcs_theme, animation_time: 300, is_completed: true, pos_right: 80, is_open: true});

		}else{
			if (quest_len < (index + 2) ){
				jQuery(".rb-popup").RBPopup({theme:_kcs_theme, animation_time: 300, data:getObj(question_arr, index), pos_right: 80, is_multiple: false, is_final: true, is_open: true });
			}else{
				jQuery(".rb-popup").RBPopup({theme:_kcs_theme, animation_time: 300,  data:getObj(question_arr, index), pos_right: 80, is_multiple: false, is_open: true });
			}
			jQuery(".rb-answer").css("display", "block");
			index = index + 1;
		}
	}
};